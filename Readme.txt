To install this OXP you need to download the music files from youtube. This can be done using a tool like youtube-dl or yt-dlp in the following way:
```
yt-dlp --ignore-errors --extract-audio --audio-format vorbis -o "%(title)s.%(ext)s" "https://www.youtube.com/playlist?list=OLAK5uy_lzw790J3eqh90cm0xR_CkOqxoeJp9c-7k"
```
The downloaded music files must be saved in the `Music` folder of this OXP.


Special thanks to:
- Svengali
- Lionel Desmond Schmitt

Licenses:

lib_orch_killed_any01.ogg - Lionel Desmond Schmitt - Fly High (Adventure Cue): CC-by
all the rest: CC-by-nc-sa-4.0

