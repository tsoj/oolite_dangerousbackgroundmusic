(function(){
"use strict";
this.name = "Dangerous Background Music";
this.author = "Tsoj";
this.copyright = "(C)2021, License:CC-by-nc-sa-4.0";
this.description = "Background music from Elite: Dangerous using Lib_Music.";
this.version = "1.0";

this._volume = 1.0

//Download files using:
//yt-dlp --ignore-errors --extract-audio --audio-format vorbis -o "%(title)s.%(ext)s" "https://www.youtube.com/playlist?list=OLAK5uy_lzw790J3eqh90cm0xR_CkOqxoeJp9c-7k"


this.startUpComplete = function(){
	delete this.startUpComplete;
	// Add event music
	var a = this.name;
	worldScripts.Lib_Music._addEventMusic({
		aegis:{
			enter:[
				{snd:"Exploration - Federal Space - Altair.ogg", dur:104, vol:this._volume},
				{snd:"Exploration - Federal Space - Tau Ceti Permutations.ogg", dur:99, vol:this._volume}					
			],
			exit:[
				{snd:"Exploration - Allied Space - To Zaonce and Beyond.ogg", dur:98, vol:this._volume},
				{snd:"Exploration - Federal Space - Arcturus Rising.ogg", dur:97, vol:this._volume},
				{snd:"Exploration - Imperial Space - Fawoal.ogg", dur:96, vol:this._volume},
				{snd:"Frameshift & Starports - Allied Starport.ogg", dur:205, vol:this._volume},
				{snd:"Frameshift & Starports - Frameshift Suite - Fourth Movement - Draco.ogg", dur:239, vol:this._volume}
			]
		},
		alert:{
			red:[
				{snd:"Combat & Extras - Combat - High Intensity - Engagement - Section 2.ogg", dur:68, vol:this._volume},
				{snd:"Combat & Extras - Combat - High Intensity - Engagement - Section 3.ogg", dur:89, vol:this._volume},
				{snd:"Combat & Extras - Combat - Low Intensity - Engagement - Section 3.ogg", dur:40, vol:this._volume},
				{snd:"Combat & Extras - Combat - Low Intensity - Engagement - Section 5.ogg", dur:46, vol:this._volume},
				{snd:"Combat & Extras - Combat - Medium Intensity - Engagement - Section 1.ogg", dur:65, vol:this._volume},
				{snd:"Combat & Extras - Combat - Medium Intensity - Engagement - Section 2.ogg", dur:73, vol:this._volume},
				{snd:"Combat & Extras - Combat - Medium Intensity - Engagement - Section 3.ogg", dur:45, vol:this._volume},
				{snd:"Combat & Extras - Combat - Medium Intensity - Engagement - Section 8.ogg", dur:110, vol:this._volume}
			]
		},
		docked:{
			any:[
				{snd:"Combat & Extras - E -D Menus.ogg", dur:218, vol:this._volume},
				{snd:"Frameshift & Starports - Anarchic Starport.ogg", dur:205, vol:this._volume}
			],
			main:[
				{snd:"Combat & Extras - E -D Menus.ogg", dur:218, vol:this._volume},
				{snd:"Frameshift & Starports - Anarchic Starport.ogg", dur:205, vol:this._volume},
				{snd:"Frameshift & Starports - Federal Starport.ogg", dur:224, vol:this._volume},
				{snd:"Frameshift & Starports - Frameshift Suite - Fifth Movement – Coma Berenices.ogg", dur:148, vol:this._volume},
			]
		},
		exitWS:{
			inter:[
				{snd:"Exploration - Anarchic Space – Keries.ogg", dur:76, vol:this._volume},
				{snd:"Exploration - Anarchic Space - Phekda.ogg", dur:84, vol:this._volume},
				{snd:"Exploration - Anarchic Space - Quator.ogg", dur:81, vol:this._volume}
			],
			standard:[
				{snd:"Exploration - Anarchic Space - Canopus.ogg", dur:74, vol:this._volume},
				{snd:"Exploration - Anarchic Space - Hyporborea.ogg", dur:75, vol:this._volume},
				{snd:"Exploration - Imperial Space - Cemiess.ogg", dur:95, vol:this._volume},
				{snd:"Exploration - Imperial Space - Delta Phoenicis.ogg", dur:90, vol:this._volume},
				{snd:"Exploration - Neutral - Fusor.ogg", dur:80, vol:this._volume},
				{snd:"Frameshift & Starports - Frameshift Suite - First Movement - Andromeda.ogg", dur:242, vol:this._volume}
			]
		},
		killed:{
			any:[{snd:"lib_orch_killed_any01.ogg", dur:48, vol:this._volume}]
		},
		launch:{
			any:[
				{snd:"Exploration - Anarchic Space - Tarach Tor.ogg", dur:89, vol:this._volume},
				{snd:"Exploration - Imperial Space - Liaedin.ogg", dur:98, vol:this._volume},
				{snd:"Frameshift & Starports - Frameshift Suite - Sixth Movement – Canes Venatici.ogg", dur:161, vol:this._volume},
				{snd:"Frameshift & Starports - Imperial Starport.ogg", dur:217, vol:this._volume}
			],
			inter:[
				{snd:"Exploration - Anarchic Space - Phekda.ogg", dur:84, vol:this._volume},
				{snd:"Exploration - Anarchic Space - Quator.ogg", dur:81, vol:this._volume}
			],
			main:[
				{snd:"Exploration - Federal Space - Altair.ogg", dur:104, vol:this._volume},
				{snd:"Exploration - Federal Space - Delta Pavonis.ogg", dur:84, vol:this._volume},
				{snd:"Exploration - Federal Space - Tau Ceti Permutations.ogg", dur:99, vol:this._volume}
			]
		},
		planetIn:{
			any: [
				{snd:"Exploration - Allied Space - Gateway.ogg", dur:100, vol:this._volume},
				{snd:"Exploration - Allied Space - Leesti.ogg", dur:98, vol:this._volume},
				{snd:"Exploration - Anarchic Space - Riedquat.ogg", dur:69, vol:this._volume},
				{snd:"Exploration - Neutral - Hoag's Object.ogg", dur:92, vol:this._volume},
				{snd:"Exploration - Allied Space - Diso.ogg", dur:75, vol:this._volume}
			],
			enterMain:[
				{snd:"Exploration - Allied Space - Gateway.ogg", dur:100, vol:this._volume},
				{snd:"Exploration - Allied Space - Leesti.ogg", dur:98, vol:this._volume},
				{snd:"Exploration - Anarchic Space - Riedquat.ogg", dur:69, vol:this._volume},
				{snd:"Exploration - Neutral - Hoag's Object.ogg", dur:92, vol:this._volume}
			],
			enterSun:[
				{snd:"Exploration - Neutral - Blue Giant.ogg", dur:82, vol:this._volume},
				{snd:"Exploration - Neutral - The Galactic Rose.ogg", dur:109, vol:this._volume}
			]
		},
		planetOut:{
			any: [
				{snd:"Exploration - Allied Space - Soholia.ogg", dur:75, vol:this._volume},
				{snd:"Exploration - Anarchic Space - Uszaa.ogg", dur:69, vol:this._volume},
				{snd:"Frameshift & Starports - Frameshift Suite - Fourth Movement - Draco.ogg", dur:239, vol:this._volume},
				{snd:"Frameshift & Starports - Frameshift Suite - Second Movement – Centaurus.ogg", dur:242, vol:this._volume},
			],
			exitMain:[
				{snd:"Exploration - Allied Space - Soholia.ogg", dur:75, vol:this._volume},
				{snd:"Exploration - Anarchic Space - Uszaa.ogg", dur:69, vol:this._volume},
				{snd:"Frameshift & Starports - Frameshift Suite - Fourth Movement - Draco.ogg", dur:239, vol:this._volume},
				{snd:"Frameshift & Starports - Frameshift Suite - Second Movement – Centaurus.ogg", dur:242, vol:this._volume},
				{snd:"Exploration - Allied Space - Lave New Alliance.ogg", dur:91, vol:this._volume},
				{snd:"Exploration - Anarchic Space - Utgaroar.ogg", dur:75, vol:this._volume},
			],
			exitSun:[
				{snd:"Exploration - Allied Space - Alioth.ogg", dur:99, vol:this._volume},
				{snd:"Exploration - Allied Space - Anayol.ogg", dur:75, vol:this._volume},
				{snd:"Frameshift & Starports - Frameshift Suite - Seventh Movement – Virgo.ogg", dur:114, vol:this._volume},
			]
		},
		scoopFuel:{
			fuel:[
				{snd:"Exploration - Neutral - Hypernova.ogg", dur:102, vol:this._volume}
			]
		}
	},a);
	worldScripts.Lib_Music._toggleGroup(a); // Turn them on

	// Playlist music
	worldScripts.Lib_Music._addChannel({name:"fight",sounds:[
		{snd:"Combat & Extras - Combat - High Intensity - Engagement - Section 1.ogg", dur:23, vol:this._volume},
		{snd:"Combat & Extras - Combat - High Intensity - Engagement - Section 4.ogg", dur:75, vol:this._volume},
		{snd:"Combat & Extras - Combat - High Intensity - Engagement - Section 5.ogg", dur:56, vol:this._volume},
		{snd:"Combat & Extras - Combat - High Intensity - Engagement - Section 6.ogg", dur:108, vol:this._volume},
		{snd:"Combat & Extras - Combat - Low Intensity - Engagement - Section 1.ogg", dur:68, vol:this._volume},
		{snd:"Combat & Extras - Combat - Low Intensity - Engagement - Section 2.ogg", dur:33, vol:this._volume},
		{snd:"Combat & Extras - Combat - Low Intensity - Engagement - Section 4.ogg", dur:82, vol:this._volume},
		{snd:"Combat & Extras - Combat - Low Intensity - Engagement - Section 6.ogg", dur:24, vol:this._volume},
		{snd:"Combat & Extras - Combat - Medium Intensity - Engagement - Section 4.ogg", dur:93, vol:this._volume},
		{snd:"Combat & Extras - Combat - Medium Intensity - Engagement - Section 5.ogg", dur:58, vol:this._volume},
		{snd:"Combat & Extras - Combat - Medium Intensity - Engagement - Section 6.ogg", dur:101, vol:this._volume},
		{snd:"Combat & Extras - Combat - Medium Intensity - Engagement - Section 7.ogg", dur:90, vol:this._volume},
	]});
	worldScripts.Lib_Music._addChannel({name:"generic",sounds:[
		{snd:"Exploration - Anarchic Space - Terra Mater.ogg", dur:87, vol:this._volume},
		{snd:"Exploration - Anarchic Space - Zelada.ogg", dur:86, vol:this._volume},
		{snd:"Exploration - Federal Space - Anlave.ogg", dur:97, vol:this._volume},
		{snd:"Exploration - Federal Space - Beta Hydri.ogg", dur:77, vol:this._volume},
		{snd:"Exploration - Federal Space - From Here to Alpha Centauri.ogg", dur:84, vol:this._volume},
		{snd:"Exploration - Federal Space - Remembering Sol.ogg", dur:100, vol:this._volume},
		{snd:"Exploration - Imperial Space - Liabeze.ogg", dur:92, vol:this._volume},
		{snd:"Exploration - Imperial Space - The Natives of Achernar.ogg", dur:93, vol:this._volume},
		{snd:"Exploration - Neutral - Lonely Pulsar.ogg", dur:87, vol:this._volume},
		{snd:"Exploration - Neutral - Magnetar.ogg", dur:70, vol:this._volume},
		{snd:"Exploration - Neutral - Neutron Star.ogg", dur:87, vol:this._volume},
		{snd:"Exploration - Neutral - The Retina Nebula.ogg", dur:94, vol:this._volume},
		{snd:"Exploration - Neutral - The Ring Nebula.ogg", dur:90, vol:this._volume},
		{snd:"Exploration - Neutral - Thorne-Żytkow Object.ogg", dur:87, vol:this._volume},
		{snd:"Exploration - Neutral - White Dwarf.ogg", dur:108, vol:this._volume},
		{snd:"Frameshift & Starports - Frameshift Suite - Fourth Movement - Draco.ogg", dur:239, vol:this._volume},
		{snd:"Frameshift & Starports - Frameshift Suite - Third Movement - Ursa Major.ogg", dur:206, vol:this._volume},
		{snd:"Frameshift & Starports - Anarchic Starport.ogg", dur:205, vol:this._volume},
		{snd:"Frameshift & Starports - Anarchic Starport.ogg", dur:205, vol:this._volume},
		{snd:"Frameshift & Starports - Federal Starport.ogg", dur:224, vol:this._volume},
	]});
};
}).call(this);
